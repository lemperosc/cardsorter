<?php

require('CardSorter.php');

// $cards = json_decode(file_get_contents('php://input'), true);
$cards = require('cards.php');

$cardSorter = new CardSorter($cards);
$cardSorter->sort();
$cards = $cardSorter->formatting();

header('Content-Type: application/json');
echo json_encode($cards, JSON_PRETTY_PRINT);
