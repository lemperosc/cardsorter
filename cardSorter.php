<?php

class CardSorter
{
    //Card array
    private $cards;

    //Sorted cars array
    private $sortedCards;

    //First card on the list. Departure card
    private $firstCard;

    //Last card of the list. Arrival Card
    private $lastCard;

    
    function __construct($cards)
    {
        $this->cards = $cards;
    }

    //Get the cards sorted
    public function getSortedCards()
    {
        return $this->sortedCards;
    }

    //Sorting procedure
    public function sort()
    {
        $this->setFirstLastCards();

        $this->removeFirstLastCardsFromCards();

        $this->addToSortedCards($this->firstCard);

        while (true) {
            foreach ($this->cards as $key => $card) {
                if (end($this->sortedCards)['to'] == $card['from']) {
                    $this->addToSortedCards($card);
                    unset($this->cards[$key]);
                }
                if (end($this->sortedCards)['to'] == $this->lastCard['from']) {
                    $this->addToSortedCards($this->lastCard);
                    return $this->sortedCards;
                }
            }
        }
    }

    //Formating boarding cards for better view
    public function formatting()
    {
        $result = [];
        foreach ($this->sortedCards as $key => $card) {
            $seat = $card['seat'] ? ' Seat ' . $card['seat'] : '';
            $baggage = $card['baggage'] ? '. ' . $card['baggage'] . '.' : '';
            $result[] = 'Take ' . $card['type'] . '. From ' . $card['from'] . ' to ' . $card['to'] . '.' . $seat . $baggage;
        }
        return $result;
    }

    /**
     * Add card to sorted cards.
     *
     * @param  array $card
     * @return void
     */
    protected function addToSortedCards($card)
    {
        $this->sortedCards[] = $card;
    }

    /**
     * Remove first and last card from cards list.
     *
     * @return void
     */
    public function removeFirstLastCardsFromCards()
    {
        foreach ($this->cards as $key => $card) {
            if ($this->firstCard == $card || $this->lastCard == $card) {
                unset($this->cards[$key]);
            }
        }
    }

    /**
     * Set first and last cards.
     *
     * @return void
     */
    public function setFirstLastCards()
    {
        $arrival = [];
        $departure = [];

        foreach ($this->cards as $key => $card) {
            $arrival[] = $card['to'];
            $departure[] = $card['from'];
        }

        foreach ($this->cards as $key => $card) {
            if (!in_array($card['from'], $arrival)) $this->firstCard = $card;
            if (!in_array($card['to'], $departure)) $this->lastCard = $card;
        }
        
    }
}
